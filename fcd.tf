provider "vra" {
  url           = var.url
  refresh_token = var.refresh_token
  insecure      = true
}

data "vra_project" "this" {
  name = var.project
}

resource "vra_block_device" "disk1" {
  capacity_in_gb = 10
  name = "terraform_vra_block_device1"
  project_id = data.vra_project.this.id

  constraints {
    mandatory  = true
    expression = "disk:fcd"
  }
  constraints {
    mandatory  = true
    expression = "cloud:vsphere"
  }

}


